class tableRow extends HTMLElement {
    constructor(){
        super();
        this.numero;
        this.nombres;
        this.edad;
    }

    static get observedAttributes(){
        return ['numero', 'nombres', 'edad'];
    }

    attributeChangedCallback(nameAttr, oldValue, newValue){
        switch(nameAttr){
            case 'numero':
                this.numero = newValue
            break;

            case 'nombres':
                this.nombres = newValue
            break;

            case 'edad':
                this.edad = newValue
            break;
        }
    }

    connectedCallback(){
        this.innerHTML = `
            <div> ${this.numero} </div>
            <div> ${this.nombres} </div>
            <div> ${this.edad} </div>
        `;
        this.style.display = 'flex';
        this.style.flexDirection = 'row'
        this.style.fontFamily = "'Roboto', sans-serif";
    }
}

window.customElements.define('table-row', tableRow);