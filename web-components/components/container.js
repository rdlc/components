class tableRow extends HTMLElement {
    constructor(){
        super();
    }

    connectedCallback(){
        this.innerHTML = `
            <div> </div>
        `;
        this.style.display = 'flex';
        this.style.flexDirection = 'row'
        this.style.fontFamily = "'Roboto', sans-serif";
    }
}

window.customElements.define('table-row', tableRow);